package tests.model;

import java.awt.Point;
import java.util.ArrayList;

import model.*;

import org.junit.Assert;
import org.junit.Test;

import tests.helper.MovesTestHelper;

public class PieceTest {

	@Test
	public void testMovePawn() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;
		
		String piecePath = "icon/White P_48x48.png";
		Piece white = new Peao(piecePath, "Peao");
		
		piecePath = "icon/Brown P_48x48.png";
		Piece brown = new Peao(piecePath, "Peao");
		

		assertMoves(MovesTestHelper.getPawnMoves(white), white.getMoves(x, y));
		assertMoves(MovesTestHelper.getPawnMoves(brown), brown.getMoves(x, y));
	}

	@Test
	public void testMoveTower() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;
		
		Piece piece = new Torre(null, null);
		//Piece piece = newPieceInstance(CLASSE_TORRE, Team.UP_TEAM);

		assertMoves(MovesTestHelper.getTowerMoves(), piece.getMoves(x, y));
	}

	@Test
	public void testMoveHorse() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;
		
		Piece piece = new Cavalo(null, null);
		//Piece piece = newPieceInstance(CLASSE_CAVALO, Team.UP_TEAM);

		assertMoves(MovesTestHelper.getHorseMoves(), piece.getMoves(x, y));
	}

	@Test
	public void testMoveBishop() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;
		
		Piece piece = new Bispo(null, null);

		assertMoves(MovesTestHelper.getBishopMoves(), piece.getMoves(x, y));
	}

	@Test
	public void testMoveKing() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;
		
		Piece piece = new Rei(null,null);
		
		//Piece piece = newPieceInstance(CLASSE_REI, Team.UP_TEAM);

		assertMoves(MovesTestHelper.getKingMoves(), piece.getMoves(x, y));
	}

	@Test
	public void testMoveQueen() throws Exception {
		int x = MovesTestHelper.POSITION_X;
		int y = MovesTestHelper.POSITION_Y;
		
		Piece piece = new Rainha(null, null);
		//Piece piece = newPieceInstance(CLASSE_RAINHA, Team.UP_TEAM);
		ArrayList<Point> moves = new ArrayList<Point>();

		moves.addAll(MovesTestHelper.getTowerMoves());
		moves.addAll(MovesTestHelper.getBishopMoves());

		assertMoves(moves, piece.getMoves(x, y));
	}

	

	private void assertMoves(ArrayList<Point> movesA, ArrayList<Point> movesB)
			throws Exception {
		if (movesA.size() != movesB.size()) {
			Assert.assertTrue(false);
			return;
		}

		for (Point point : movesA) {
			if (!movesB.contains(point)) {
				Assert.assertTrue(false);
				return;
			}
		}

		Assert.assertTrue(true);
	}
}
