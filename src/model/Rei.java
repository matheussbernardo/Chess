
package model;

import java.awt.Point;
import java.util.ArrayList;

public class Rei extends Piece{

	public Rei(String imagePath, String typeOfObject) {
		super(imagePath, typeOfObject);
	}

	public Rei(String imagePath) {
		super(imagePath);
	}
	
	@Override
	public ArrayList<Point> getMoves(int x, int y) {
		ArrayList<Point> movesRei = new ArrayList<>();
		if(!(x == 0)) {
			Point pointC = new Point(x-1, y);
			movesRei.add(pointC);
			if(!(y == 7)) {
				Point pointCD = new Point(x-1, y+1);
				movesRei.add(pointCD);
			}
			if(!(y == 0)) {
				Point pointCE = new Point(x-1, y-1);		
				movesRei.add(pointCE);
			}
		}
		if(!(x == 7)) {
			Point pointB = new Point(x+1, y);
			movesRei.add(pointB);
			if(!(y == 7)) {
				Point pointBD = new Point(x+1, y+1);
				movesRei.add(pointBD);
			}
			if(!(y == 0)) {
				Point pointBE = new Point(x+1, y-1);
				movesRei.add(pointBE);
			}
		}
		if(!(y == 7)) {		
			Point pointD = new Point(x, y+1);
			movesRei.add(pointD);
		}
		if(!(y == 0)) {		
			Point pointE = new Point(x,y-1);
			movesRei.add(pointE);
		}
		return movesRei;
	}
}
