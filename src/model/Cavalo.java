package model;

import java.awt.Point;
import java.util.ArrayList;

public class Cavalo extends Piece{
	
	
	public Cavalo(String imagePath, String typeOfObject) {
		super(imagePath, typeOfObject);
	}

	public Cavalo(String imagePath) {
		super(imagePath);
	}

	@Override
	public ArrayList<Point> getMoves(int x, int y) {
		ArrayList<Point> movesCavalo = new ArrayList<>();	

		if( !(y == 6 || y ==7) ) {		
			if(!(x == 0)) {
				Point pointDC = new Point(x-1, y+2);
				movesCavalo.add(pointDC);
			}		
			if(!(x == 7) ) {
				Point pointDB = new Point(x+1, y+2);
				movesCavalo.add(pointDB);
			}			
		}
	
		if( !(y == 0 || y == 1) ) {
			if(!(x == 0)) {
				Point pointEC = new Point(x-1, y-2);
				movesCavalo.add(pointEC);
			}
			if(!(x == 7) ) {
				Point pointEB = new Point(x+1, y-2);
				movesCavalo.add(pointEB);
			}
		}

		if(!(x == 0 || x == 1) ) {
			if(!(y == 7))
			{
				Point pointCD = new Point(x-2, y+1);
				movesCavalo.add(pointCD);
			}
			if(!(y == 0))
			{
				Point pointCE = new Point(x-2, y-1);
				movesCavalo.add(pointCE);
			}
		}
		
		if( !(x == 7 || x == 6) ) {	
			if(!(y == 7))
			{
				Point pointBD = new Point(x+2, y+1);	
				movesCavalo.add(pointBD);
			}
			if(!(y == 0) ) {
				Point pointBE = new Point(x+2, y-1);
				movesCavalo.add(pointBE);
			}
		}	
		
		return movesCavalo;
	}
}
