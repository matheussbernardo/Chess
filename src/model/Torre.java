package model;

import java.awt.Point;
import java.util.ArrayList;

public class Torre extends Piece {
	
	public Torre(String imagePath, String typeOfObject) {
		super(imagePath, typeOfObject);
	}

	public Torre(String imagePath) {
		super(imagePath);
	}
	@Override
	public ArrayList<Point> getMoves(int x, int y) {
		
		ArrayList<Point> movesTorre = new ArrayList<>();
		
		for(int i = y+1; i<8; i++) {
			Point pointD = new Point(x, i);
			
			
			movesTorre.add(pointD);
		}
		for(int i = y-1; i>=0; i--) {
			Point pointE = new Point(x, i);
			
			movesTorre.add(pointE);
		}
		
		for(int i = x+1; i<8; i++) {
			Point pointC = new Point(i, y);
			
			movesTorre.add(pointC);
		}
		for(int i = x-1; i>=0; i--) {
			Point pointB = new Point(i, y);
			
			movesTorre.add(pointB);
		}
		return movesTorre;
		
	}
}
