package model;

import java.awt.Point;
import java.util.ArrayList;

public abstract class Piece {
	private String imagePath;
	private String typeOfObject;
	
	public Piece(String imagePath) {
		super();
		this.imagePath = imagePath;
	}

	public Piece(String imagePath, String typeOfObject) {
		super();
		this.imagePath = imagePath;
		this.typeOfObject = typeOfObject;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getTypeOfObject() {
		return typeOfObject;
	}

	public void setTypeOfObject(String typeOfObject) {
		this.typeOfObject = typeOfObject;
	}


	public abstract ArrayList<Point> getMoves(int x, int y);
	
}
