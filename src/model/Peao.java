package model;

import java.awt.Point;
import java.util.ArrayList;

public class Peao extends Piece{

	public Peao(String imagePath, String typeOfObject) {
		super(imagePath, typeOfObject);
	}

	public Peao(String imagePath) {
		super(imagePath);
	}
	
	@Override
	public ArrayList<Point> getMoves(int x, int y) {
		if(this.getImagePath().contains("White")) {
			ArrayList<Point> movesPeao = new ArrayList<>();
			
			if(x == 6) {
				Point pointF = new Point(x - 1, y);
				movesPeao.add(pointF);
				
				Point pointFF = new Point(x - 2 , y);
				movesPeao.add(pointFF);
				
				Point pointEsquerdo = new Point(x-1, y-1);
				movesPeao.add(pointEsquerdo);
				
				Point pointDireito = new Point(x-1, y+1);
				movesPeao.add(pointDireito);
			}
			else {
				Point pointEsquerdo = new Point(x-1, y-1);
				movesPeao.add(pointEsquerdo);
				
				Point pointDireito = new Point(x-1, y+1);
				movesPeao.add(pointDireito);
				
				Point pointFrente = new Point(x-1, y);
				movesPeao.add(pointFrente);
			}
			
			return movesPeao;
		}
		else {
			ArrayList<Point> movesPeao = new ArrayList<>();
			
			if(x == 1) {
				Point pointF = new Point(x + 1, y);
				movesPeao.add(pointF);
				
				Point pointFF = new Point(x + 2 , y);
				movesPeao.add(pointFF);
				
				Point pointEsquerdo = new Point(x+1, y+1);
				movesPeao.add(pointEsquerdo);
				
				Point pointDireito = new Point(x+1, y-1);
				movesPeao.add(pointDireito);
			}
			else {
				Point pointEsquerdo = new Point(x+1, y+1);
				movesPeao.add(pointEsquerdo);
				
				Point pointDireito = new Point(x+1, y-1);
				movesPeao.add(pointDireito);
				
				Point pointFrente = new Point(x+1, y);
				movesPeao.add(pointFrente);
			}
			
			return movesPeao;
			
		}
		
	}
}
