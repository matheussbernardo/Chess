package control;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

import model.Piece;
import model.Square;
import model.Square.SquareEventListener;
import model.Torre;
import model.outOfPossibleSquaresException;

public class SquareControl implements SquareEventListener {

	public static final int ROW_NUMBER = 8;
	public static final int COL_NUMBER = 8;

	public static final Color DEFAULT_COLOR_ONE = Color.WHITE;
	public static final Color DEFAULT_COLOR_TWO = Color.GRAY;
	public static final Color DEFAULT_COLOR_HOVER = Color.BLUE;
	public static final Color DEFAULT_COLOR_SELECTED = Color.GREEN;
	public static final Color DEFAULT_COLOR_MOVE = new Color(0x66B0FF);
	
	public static final Square EMPTY_SQUARE = null;

	private Color colorOne;
	private Color colorTwo;
	private Color colorHover;
	private Color colorSelected;
	private Color colorMove;
	
	private Square selectedSquare;
	private ArrayList<Square> squareList;
	private ArrayList<Square> possibleSquares;

	public SquareControl() {
		this(DEFAULT_COLOR_ONE, DEFAULT_COLOR_TWO, DEFAULT_COLOR_HOVER,
				DEFAULT_COLOR_SELECTED, DEFAULT_COLOR_MOVE);
	}

	public SquareControl(Color colorOne, Color colorTwo, Color colorHover,
			Color colorSelected, Color colorMove) {
		this.colorOne = colorOne;
		this.colorTwo = colorTwo;
		this.colorHover = colorHover;
		this.colorSelected = colorSelected;
		this.colorMove = colorMove;
		
		this.squareList = new ArrayList<>();
		this.possibleSquares = new ArrayList<>();
		
		createSquares();
	}

	public void resetColor(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.setColor(getGridColor(row, col));
	}

	@Override
	public void onHoverEvent(Square square) {
		System.out.println("X = " + square.getPosition().x + "Y = " + square.getPosition().y);
		square.setColor(this.colorHover);
	}

	@Override
	public void onSelectEvent(Square square) throws outOfPossibleSquaresException {
		if (haveSelectedCellPanel()) {
			if(!possibleSquares.contains(square) && !this.selectedSquare.equals(square))
			{
				throw new outOfPossibleSquaresException("QUADRADO INDISPONIVEL");
			}
			if (!this.selectedSquare.equals(square) && possibleSquares.contains(square)) { 
				moveContentOfSelectedSquare(square);
				possibleSquares.clear();
			} 
			else {
				unselectSquare(square);
			}
		} 
		else {
			selectSquare(square);
		}
	}

	@Override
	public void onOutEvent(Square square) {
		
		if (this.selectedSquare != square && !possibleSquares.contains(square) ) {	
			resetColor(square);
		} 
		else {
			if(this.selectedSquare == square)
				square.setColor(this.colorSelected);		
			else
				square.setColor(new Color(0x66B0FF));
		}
		
	}

	public Square getSquare(int row, int col) {
		return this.squareList.get((row * COL_NUMBER) + col);
	}

	public ArrayList<Square> getSquareList() {
		return this.squareList;
	}

	public Color getGridColor(int row, int col) {
		if ((row + col) % 2 == 0) {
			return this.colorOne;
		} else {
			return this.colorTwo;
		}
	}

	private void addSquare() {
		Square square = new Square();
		this.squareList.add(square);
		resetColor(square);
		resetPosition(square);
		square.setSquareEventListener(this);
	}

	private void resetPosition(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.getPosition().setLocation(row, col);
	}

	private boolean haveSelectedCellPanel() {
		return this.selectedSquare != EMPTY_SQUARE;
	}

	private void moveContentOfSelectedSquare(Square square) {
		square.setPiece(this.selectedSquare.getPiece());
		this.selectedSquare.removePiece();
		unselectSquare(square);
	}

	private void selectSquare(Square square) {
		if (square.havePiece()) {
			this.selectedSquare = square;
			this.selectedSquare.setColor(this.colorSelected);
			this.showPossibleMoves(this.selectedSquare);
		}
	}

	private void unselectSquare(Square square) {
		resetColor(this.selectedSquare);
		
		for(int i = 0; i<possibleSquares.size(); i++)
			resetColor(possibleSquares.get(i));
		possibleSquares.clear();
		this.selectedSquare = EMPTY_SQUARE;
	}

	private void createSquares() {
		for (int i = 0; i < (ROW_NUMBER * COL_NUMBER); i++) {
			addSquare();
		}
	}
	
	
	private void showPossibleMovesPeaoB(Square square) {
		ArrayList<Point> movesPeao = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		
		for (int i = 0; i <movesPeao.size(); i++) {
			Square squares = getSquare(movesPeao.get(i).x, movesPeao.get(i).y);
			Square squareEsquerdo = getSquare(square.getPosition().x+1, square.getPosition().y+1);
			Square squareDireito = getSquare(square.getPosition().x+1, square.getPosition().y-1);
			Square squareF = getSquare(square.getPosition().x+1, square.getPosition().y);

			if( ( square.getPosition().y != 0 && squares == squareDireito && squareDireito.havePiece() )   
					||	(square.getPosition().y != 7 && squares == squareEsquerdo && squareEsquerdo.havePiece() ) ) {

				if(squares.getPiece().getImagePath().contains("White")) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);
				}
			}
			if(squares != squareDireito && squares != squareEsquerdo) {		

				if(!squares.havePiece() && !squareF.havePiece()) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);
				}	
			}
		}
		
	}
	private void showPossibleMovesPeaoW(Square square) {
		ArrayList<Point> movesPeao = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		
		for (int i = 0; i <movesPeao.size(); i++) {
			
			Square squares = getSquare(movesPeao.get(i).x, movesPeao.get(i).y);
			Square squareEsquerdo = getSquare(square.getPosition().x-1, square.getPosition().y-1);
			Square squareDireito = getSquare(square.getPosition().x-1, square.getPosition().y+1);
			Square squareF = getSquare(square.getPosition().x-1, square.getPosition().y);
			if( ( square.getPosition().y != 7 && squares == squareDireito && squareDireito.havePiece() )   
					||	(square.getPosition().y != 0 && squares == squareEsquerdo && squareEsquerdo.havePiece() ) ) {

				if(squares.getPiece().getImagePath().contains("Brown")) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);
				}
			}
			if(squares != squareDireito && squares != squareEsquerdo) {		

				if(!squares.havePiece() && !squareF.havePiece()) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);
				}	
			}
			
			
		}
		
	}
	private void showPossibleMovesCavalo(Square square) {
		
		ArrayList<Point> movesCavalo = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		
		for (int i = 0; i <movesCavalo.size(); i++) {			
			Square squares = getSquare(movesCavalo.get(i).x, movesCavalo.get(i).y);							
			
			if(square.getPiece().getImagePath().contains("White")) {
				if(!squares.havePiece()) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);
				}
				else {
					if(squares.getPiece().getImagePath().contains("Brown")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}
			}
			
			if(square.getPiece().getImagePath().contains("Brown")) {
				if(!squares.havePiece()) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);
				}
				else {
					if(squares.getPiece().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}		
			}
			
		}
		
	}
	
	private void showPossibleMovesTorreB(Square square) {
		
		Torre torre = (Torre) square.getPiece();
		
		ArrayList<Point> movesTorre = torre.getMoves(square.getPosition().x, square.getPosition().y);			
		
		int i = 0;
		Square squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);		
		while( squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y > square.getPosition().y)  {		
			
			if(squares.havePiece())
			{
				System.out.println(squares.getPosition().x + "  POSITION DO BREAK  DIREITA" + squares.getPosition().y);
				if(square.getPiece().getImagePath().contains("White")) {
					if(squares.getPiece().getImagePath().contains("Brown")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagePath().contains("Brown")) {
					if(squares.getPiece().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y > square.getPosition().y) {			
					i++;
					squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSITION QUE PINTA  DIREITA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
		}
		System.out.println("I = " + i);
		squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
		while( squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y < square.getPosition().y)  {		
			
			if(squares.havePiece())
			{
				System.out.println(squares.getPosition().x + "  POSITION DO BREAK  ESQUERDA" + squares.getPosition().y);
				if(square.getPiece().getImagePath().contains("White")) {
					if(squares.getPiece().getImagePath().contains("Brown")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagePath().contains("Brown")) {
					if(squares.getPiece().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y < square.getPosition().y) {
					i++;
					squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSITION QUE PINTA ESQUERDA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
		}	
		
		
		///////////
		squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
		while( squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x >  square.getPosition().x)  {		
			
			if(squares.havePiece())
			{
				System.out.println(squares.getPosition().x + "  POSITION DO BREAK BAIZO" + squares.getPosition().y);
				if(square.getPiece().getImagePath().contains("White")) {
					if(squares.getPiece().getImagePath().contains("Brown")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagePath().contains("Brown")) {
					if(squares.getPiece().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x > square.getPosition().x) {
					i++;
					squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSITION QUE PINTA  BAIXO" + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
		}
		
		//////////
		squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
		while( squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x < square.getPosition().x)  {		
			
			if(squares.havePiece())
			{
				System.out.println(squares.getPosition().x + "  POSITION DO BREAK CIMA" + squares.getPosition().y);
				if(square.getPiece().getImagePath().contains("White")) {
					if(squares.getPiece().getImagePath().contains("Brown")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagePath().contains("Brown")) {
					if(squares.getPiece().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSITION QUE PINTA  CIMA" + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesTorre.get(i).x, movesTorre.get(i).y);
		}
				
	}
	private void showPossibleMovesBispoB(Square square) {

		ArrayList<Point> movesBispo = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		
		int i = 0;
		Square squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);	
		
		while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y > square.getPosition().y)  {		
			
			if(squares.havePiece())
			{
				System.out.println(squares.getPosition().x + "  POSITION DO BREAK  DIREITA" + squares.getPosition().y);
				if(square.getPiece().getImagePath().contains("White")) {
					if(squares.getPiece().getImagePath().contains("Brown")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagePath().contains("Brown")) {
					if(squares.getPiece().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y > square.getPosition().y) {			
					i++;				
					squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);		
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSITION QUE PINTA  DIREITA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		}	
		
		squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y < square.getPosition().y)  {		
			
			if(squares.havePiece())
			{
				System.out.println(squares.getPosition().x + "  POSITION DO BREAK  ESQUERDA" + squares.getPosition().y);
				if(square.getPiece().getImagePath().contains("White")) {
					if(squares.getPiece().getImagePath().contains("Brown")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagePath().contains("Brown")) {
					if(squares.getPiece().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}		
				while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y < square.getPosition().y)  {		
					i++;
					squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSITION QUE PINTA ESQUERDA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		}
		
		squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		while(squares.getPosition().x <  square.getPosition().x && squares.getPosition().y > square.getPosition().y )  {		
			
			if(squares.havePiece())
			{
				System.out.println(squares.getPosition().x + "  POSITION DO BREAK  ESQUERDA" + squares.getPosition().y);
				if(square.getPiece().getImagePath().contains("White")) {
					if(squares.getPiece().getImagePath().contains("Brown")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagePath().contains("Brown")) {
					if(squares.getPiece().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x <  square.getPosition().x && squares.getPosition().y > square.getPosition().y )  {		
					i++;
					squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSITION QUE PINTA ESQUERDA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		}
		
		squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		while( squares.getPosition().x <  square.getPosition().x && squares.getPosition().y < square.getPosition().y )  {		
			
			if(squares.havePiece())
			{
				System.out.println(squares.getPosition().x + "  POSITION DO BREAK  ESQUERDA" + squares.getPosition().y);
				if(square.getPiece().getImagePath().contains("White")) {
					if(squares.getPiece().getImagePath().contains("Brown")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagePath().contains("Brown")) {
					if(squares.getPiece().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSITION QUE PINTA ESQUERDA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesBispo.get(i).x, movesBispo.get(i).y);
		}
		
	}
	private void showPossibleMovesRainhaB(Square square) {		

		ArrayList<Point> movesRainha = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);


		int i = 0;
		Square squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);		
		while( squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y > square.getPosition().y)  {		

			if(squares.havePiece())
			{
				System.out.println(squares.getPosition().x + "  POSITION DO BREAK  DIREITA" + squares.getPosition().y);
				if(square.getPiece().getImagePath().contains("White")) {
					if(squares.getPiece().getImagePath().contains("Brown")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagePath().contains("Brown")) {
					if(squares.getPiece().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y > square.getPosition().y) {			
					i++;
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSITION QUE PINTA  DIREITA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}
		System.out.println("I = " + i);
		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		while( squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y < square.getPosition().y)  {		

			if(squares.havePiece())
			{
				System.out.println(squares.getPosition().x + "  POSITION DO BREAK  ESQUERDA" + squares.getPosition().y);
				if(square.getPiece().getImagePath().contains("White")) {
					if(squares.getPiece().getImagePath().contains("Brown")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagePath().contains("Brown")) {
					if(squares.getPiece().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x == square.getPosition().x &&  squares.getPosition().y < square.getPosition().y) {
					i++;
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSITION QUE PINTA ESQUERDA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}	


		///////////
		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		while( squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x >  square.getPosition().x)  {		

			if(squares.havePiece())
			{
				System.out.println(squares.getPosition().x + "  POSITION DO BREAK BAIZO" + squares.getPosition().y);
				if(square.getPiece().getImagePath().contains("White")) {
					if(squares.getPiece().getImagePath().contains("Brown")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagePath().contains("Brown")) {
					if(squares.getPiece().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x > square.getPosition().x) {
					i++;
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSITION QUE PINTA  BAIXO" + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}

		//////////
		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		while( squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x < square.getPosition().x)  {		

			if(squares.havePiece())
			{
				System.out.println(squares.getPosition().x + "  POSITION DO BREAK CIMA" + squares.getPosition().y);
				if(square.getPiece().getImagePath().contains("White")) {
					if(squares.getPiece().getImagePath().contains("Brown")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagePath().contains("Brown")) {
					if(squares.getPiece().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				while( squares.getPosition().y == square.getPosition().y &&  squares.getPosition().x < square.getPosition().x)  {
					i++;
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSITION QUE PINTA  CIMA" + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}

		
		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);	

		while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y > square.getPosition().y)  {		

			if(squares.havePiece())
			{
				System.out.println(squares.getPosition().x + "  POSITION DO BREAK  DIREITA" + squares.getPosition().y);
				if(square.getPiece().getImagePath().contains("White")) {
					if(squares.getPiece().getImagePath().contains("Brown")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagePath().contains("Brown")) {
					if(squares.getPiece().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y > square.getPosition().y) {			
					i++;				
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);		
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSITION QUE PINTA  DIREITA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}	

		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y < square.getPosition().y)  {		

			if(squares.havePiece())
			{
				System.out.println(squares.getPosition().x + "  POSITION DO BREAK  ESQUERDA" + squares.getPosition().y);
				if(square.getPiece().getImagePath().contains("White")) {
					if(squares.getPiece().getImagePath().contains("Brown")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagePath().contains("Brown")) {
					if(squares.getPiece().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}		
				while(squares.getPosition().x >  square.getPosition().x && squares.getPosition().y < square.getPosition().y)  {		
					i++;
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSITION QUE PINTA ESQUERDA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}

		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		while(squares.getPosition().x <  square.getPosition().x && squares.getPosition().y > square.getPosition().y )  {		

			if(squares.havePiece())
			{
				System.out.println(squares.getPosition().x + "  POSITION DO BREAK  ESQUERDA" + squares.getPosition().y);
				if(square.getPiece().getImagePath().contains("White")) {
					if(squares.getPiece().getImagePath().contains("Brown")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagePath().contains("Brown")) {
					if(squares.getPiece().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				while(squares.getPosition().x <  square.getPosition().x && squares.getPosition().y > square.getPosition().y )  {		
					i++;
					squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSITION QUE PINTA ESQUERDA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}

		squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		while( squares.getPosition().x <  square.getPosition().x && squares.getPosition().y < square.getPosition().y )  {		

			if(squares.havePiece())
			{
				System.out.println(squares.getPosition().x + "  POSITION DO BREAK  ESQUERDA" + squares.getPosition().y);
				if(square.getPiece().getImagePath().contains("White")) {
					if(squares.getPiece().getImagePath().contains("Brown")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}	
				if(square.getPiece().getImagePath().contains("Brown")) {
					if(squares.getPiece().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);	
					}	
				}
				break;
			}
			else {
				System.out.println(squares.getPosition().x + " POSITION QUE PINTA ESQUERDA " + squares.getPosition().y);
				squares.setColor(this.colorMove);
				possibleSquares.add(squares);
			}
			i++;
			squares = getSquare(movesRainha.get(i).x, movesRainha.get(i).y);
		}

	}
	
	private void showPossibleMovesReiB(Square square) {
		ArrayList<Point> movesRei = square.getPiece().getMoves(square.getPosition().x, square.getPosition().y);
		for (int i = 0; i <movesRei.size(); i++) {
			
			Square squares = getSquare(movesRei.get(i).x, movesRei.get(i).y);
			if(square.getPiece().getImagePath().contains("White")) {
				if(!squares.havePiece()) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);
				}
				else {
					if(squares.getPiece().getImagePath().contains("Brown")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}
			}
			
			if(square.getPiece().getImagePath().contains("Brown")) {
				if(!squares.havePiece()) {
					squares.setColor(this.colorMove);
					possibleSquares.add(squares);
				}
				else {
					if(squares.getPiece().getImagePath().contains("White")) {
						squares.setColor(this.colorMove);
						possibleSquares.add(squares);
					}
				}		
			}	
			
		}// fim for
	}// fim metodo
	
	private void showPossibleMoves(Square square) {
		Piece piece = square.getPiece();
		
		if(piece.getTypeOfObject().equalsIgnoreCase("Peao")) {
			if(piece.getImagePath().contains("White"))
				showPossibleMovesPeaoW(square);
			if(piece.getImagePath().contains("Brown"))
				showPossibleMovesPeaoB(square);
		}
		
		if(piece.getTypeOfObject().equalsIgnoreCase("Torre")) {	
				showPossibleMovesTorreB(square);				
		}
		
		if(piece.getTypeOfObject().equalsIgnoreCase("Cavalo")) {	
				showPossibleMovesCavalo(square);
		}
		
		if(piece.getTypeOfObject().equalsIgnoreCase("Bispo")) {
				showPossibleMovesBispoB(square);
		}
		
		if(piece.getTypeOfObject().equalsIgnoreCase("Rainha")) {
				showPossibleMovesRainhaB(square);
		}
		
		if(piece.getTypeOfObject().equalsIgnoreCase("Rei")) {
				showPossibleMovesReiB(square);
		}
		
		
	}
}
