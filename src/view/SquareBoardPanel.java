package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import model.Bispo;
import model.Cavalo;
import model.Peao;
import model.Piece;
import model.Rainha;
import model.Rei;
import model.Square;
import model.Torre;
import control.SquareControl;

public class SquareBoardPanel extends JPanel {

	private static final long serialVersionUID = 7332850110063699836L;

	private SquareControl squareControl;
	private ArrayList<SquarePanel> squarePanelList;

	public SquareBoardPanel() {
		setLayout(new GridBagLayout());
		this.squarePanelList = new ArrayList<SquarePanel>();

		initializeSquareControl();
		initializeGrid();
		initializePiecesInChess();
	}

	private void initializeSquareControl() {
		Color colorOne = Color.WHITE;
		Color colorTwo = Color.GRAY;
		Color colorHover = Color.BLUE;
		Color colorSelected = Color.GREEN;
		Color colorMove = new Color(0x66B0FF);

		this.squareControl = new SquareControl(colorOne, colorTwo, colorHover,
				colorSelected, colorMove);
	}

	private void initializeGrid() {
		GridBagConstraints gridBag = new GridBagConstraints();

		Square square;
		for (int i = 0; i < this.squareControl.getSquareList().size(); i++) {
			square = this.squareControl.getSquareList().get(i);
			gridBag.gridx = square.getPosition().y;
			gridBag.gridy = square.getPosition().x;

			SquarePanel squarePanel = new SquarePanel(square);

			add(squarePanel, gridBag);
			this.squarePanelList.add(squarePanel);
		}

	}

	private void initializePiecesInChess() {
		String piecePath = "icon/Brown P_48x48.png";
		Piece umPeao = new Peao(piecePath,"Peao");
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(1, i).setPiece(umPeao);
			
		}
		piecePath = "icon/Brown R_48x48.png";
		Piece umaTorre = new Torre(piecePath,"Torre");
		this.squareControl.getSquare(0, 0).setPiece(umaTorre);
		this.squareControl.getSquare(0, 7).setPiece(umaTorre);

		piecePath = "icon/Brown N_48x48.png";
		Piece umCavalo = new Cavalo(piecePath, "Cavalo");
		this.squareControl.getSquare(0, 1).setPiece(umCavalo);
		this.squareControl.getSquare(0, 6).setPiece(umCavalo);

		piecePath = "icon/Brown B_48x48.png";
		Piece umBispo = new Bispo(piecePath,"Bispo");
		this.squareControl.getSquare(0, 2).setPiece(umBispo);
		this.squareControl.getSquare(0, 5).setPiece(umBispo);

		piecePath = "icon/Brown Q_48x48.png";
		Piece umaRainha = new Rainha(piecePath, "Rainha"); 
		this.squareControl.getSquare(0, 4).setPiece(umaRainha);

		piecePath = "icon/Brown K_48x48.png";
		Piece umRei = new Rei(piecePath, "Rei");
		this.squareControl.getSquare(0, 3).setPiece(umRei);

		
		piecePath = "icon/White P_48x48.png";
		Piece umPeaoW = new Peao(piecePath, "Peao");
		for (int i = 0; i < SquareControl.COL_NUMBER; i++) {
			this.squareControl.getSquare(6, i).setPiece(umPeaoW);
		}
		
		piecePath = "icon/White R_48x48.png";
		Piece umaTorreW = new Torre(piecePath, "Torre");
		this.squareControl.getSquare(7, 0).setPiece(umaTorreW);
		this.squareControl.getSquare(7, 7).setPiece(umaTorreW);

		piecePath = "icon/White N_48x48.png";
		Piece umCavaloW = new Cavalo(piecePath, "Cavalo");
		this.squareControl.getSquare(7, 1).setPiece(umCavaloW);
		this.squareControl.getSquare(7, 6).setPiece(umCavaloW);

		piecePath = "icon/White B_48x48.png";
		Piece umBispoW = new Bispo(piecePath, "Bispo");
		this.squareControl.getSquare(7, 2).setPiece(umBispoW);
		this.squareControl.getSquare(7, 5).setPiece(umBispoW);

		piecePath = "icon/White Q_48x48.png";
		Piece umaRainhaW = new Rainha(piecePath, "Rainha");
		this.squareControl.getSquare(7, 4).setPiece(umaRainhaW);
	
		piecePath = "icon/White K_48x48.png";
		Piece umReiW = new Rei(piecePath, "Rei");
		this.squareControl.getSquare(7, 3).setPiece(umReiW);
	}
}
